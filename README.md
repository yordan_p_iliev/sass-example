This was a frond-end job application task project. I was given a PSD file and instructions.

The webpage is responsive. There was no provided design for the mobile layout.

The webpage contains an element with sliding images and changing text that switches every ten seconds and can be controlled by the dots under "Have a Look".

The information under "Our Featured Professionals" can be navigated with the arrows and shows four people on desktop and two people on mobile.